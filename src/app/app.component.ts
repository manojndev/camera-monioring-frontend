import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import {camservice} from "../app/service/service"
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';




@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {





  


















  public selectedIndex = 0;
  public appPages = [
/*     {
      title: 'My account',
      url: '/folder/Inbox',
      icon: 'add'
    },
    {
      title: 'My account',
      url: '/folder/Outbox',
      icon: 'mail'
    } */
  ];






  public labels = [ ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private alertController: AlertController,
    private camserv:camservice,
    private storage: Storage,
    private loadingController:LoadingController
  ) {
    this.initializeApp();
  }



  //////////////////////////////////////////////all type of alert/////////////

  async presentAlertMultipleButtons() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['Cancel', 'Open Modal', 'Delete'],
      backdropDismiss: false
    });

    await alert.present();
  }

 ////////////////////////////////////////////////////////////////////


getcamera(username){


this.camserv.getcamera(username);
console.log(username);

}




////////////////////////////////////////////////////////////////////////////////////////////////////

async failed(text) {
  const alert = await this.alertController.create({
  
    header: text,
    backdropDismiss: false,
    inputs: [
     /*  {
        name: 'name1',
        type: 'text',
        placeholder: 'username'

      },
      {
        name: 'name2',
        type: 'password',
        id: 'name2-id',
        //value: 'hello',
        placeholder: 'password'
      }, */
      // multiline input.
     /*  {
        name: 'paragraph',
        id: 'paragraph',
        type: 'textarea',
        placeholder: 'Placeholder 3'
      },
      {
        name: 'name3',
        value: 'http://ionicframework.com',
        type: 'url',
        placeholder: 'Favorite site ever'
      },
      // input date with min & max
      {
        name: 'name4',
        type: 'date',
        min: '2017-03-01',
        max: '2018-01-12'
      },
      // input date without min nor max
      {
        name: 'name5',
        type: 'date'
      },
      {
        name: 'name6',
        type: 'number',
        min: -5,
        max: 10
      },
      {
        name: 'name7',
        type: 'number'
      } */
    ],
    buttons: [
   /*    {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
          console.log('Confirm Cancel');
        } 
      }*/, {
        text: 'Tryagain',
        handler: (data) => {
          

this.presentAlertPrompt();
 
        
     

        
        }
      }
    ]
  });

  await alert.present();
}


///////////////////////////////////////////////////////////sucess alert prompt////////////////////////////////////////
async sucess(text) {
  const alert = await this.alertController.create({
  
    header: text,
    backdropDismiss: false,
    inputs: [
     /*  {
        name: 'name1',
        type: 'text',
        placeholder: 'username'

      },
      {
        name: 'name2',
        type: 'password',
        id: 'name2-id',
        //value: 'hello',
        placeholder: 'password'
      }, */
      // multiline input.
     /*  {
        name: 'paragraph',
        id: 'paragraph',
        type: 'textarea',
        placeholder: 'Placeholder 3'
      },
      {
        name: 'name3',
        value: 'http://ionicframework.com',
        type: 'url',
        placeholder: 'Favorite site ever'
      },
      // input date with min & max
      {
        name: 'name4',
        type: 'date',
        min: '2017-03-01',
        max: '2018-01-12'
      },
      // input date without min nor max
      {
        name: 'name5',
        type: 'date'
      },
      {
        name: 'name6',
        type: 'number',
        min: -5,
        max: 10
      },
      {
        name: 'name7',
        type: 'number'
      } */
    ],
    buttons: [
   /*    {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
          console.log('Confirm Cancel');
        } 
      }*/, {
        text: 'OK',
        handler: (data) => {
          

 
        
     

        
        }
      }
    ]
  });

  await alert.present();
}


















///////////////////////////////////////////////////success alert prompt////////////////////////////////////









/////////////////////////////////////////////////////////////////////////////////////////////////////









  async presentAlertPrompt() {
    const alert = await this.alertController.create({
    
      header: 'Login',
      backdropDismiss: false,
      inputs: [
        {
          name: 'name1',
          type: 'text',
          placeholder: 'username'
  
        },
        {
          name: 'name2',
          type: 'password',
          id: 'name2-id',
          //value: 'hello',
          placeholder: 'password'
        },
        // multiline input.
       /*  {
          name: 'paragraph',
          id: 'paragraph',
          type: 'textarea',
          placeholder: 'Placeholder 3'
        },
        {
          name: 'name3',
          value: 'http://ionicframework.com',
          type: 'url',
          placeholder: 'Favorite site ever'
        },
        // input date with min & max
        {
          name: 'name4',
          type: 'date',
          min: '2017-03-01',
          max: '2018-01-12'
        },
        // input date without min nor max
        {
          name: 'name5',
          type: 'date'
        },
        {
          name: 'name6',
          type: 'number',
          min: -5,
          max: 10
        },
        {
          name: 'name7',
          type: 'number'
        } */
      ],
      buttons: [
         {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
           
            this.storage.get("users").then(val=>{

              console.log(val.users.length+"this is the value the length");

              if ( val.users.length> 0) {
                // the array is defined and has at least one element
          console.log("closing th dialog");
             

            }else{
             this.presentAlertPrompt();

            }
        
        
            })




          } 
        }, {
          text: 'Login',
          handler: (data) => {

            console.log(data);
if(data.name1===""||data.name2===""){

this.failed("please enter the username/password");

}
else{

  ////////////////checking and storing the login to local storage///////////////////////
  this.camserv.checklogin(data);

  this.camserv.getloginupdatelistener().subscribe(val=>{

if(val===true){
  this.storage.get("users").then(val=>{
    let users=val.users;
    users.push(data.name1);
 
  
  

    


     this.storage.set("users",{
      
      "users":users
           }); 

           this.updatesidemenu();              

  })
  this.updatesidemenu(); 



}

else if(val===false){

  this.failed("please check username and password and try/again");

}




  })


 ////////////////checking and storing the login to local storage///////////////////////



}




   
          
       

          
          }
        }
      ]
    });

    await alert.present();
  }





/////////////////////////////////////////all type of alert/////////////////


updatesidemenu(){
  

console.log("this worked isde sidemenu");
  this.storage.get("users").then(val=>{

var obj=JSON.parse(JSON.stringify(val));

var tempar=[];


for( let v of obj.users){

  tempar.push( {
    title: v,
    url: '/folder/'+v,
    icon: 'videocam'
  })

  
}

this.appPages=tempar;



})

}





















  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

 




console.log("storage happens in this place");

      this.storage.get("users").then(val=>{
/////////////////////////////////////////////////////checking prompt///////////////////////////////////



        if ( val==null || val.users.length==0 ) {

          this.presentAlertPrompt();

        }

        
////////////////////////////////////////////////////checking prompt////////////////////////////////////////////


       console.log(JSON.stringify(val)+"   this is the value on initilization");
        if ( val==null || val==undefined) {

        this.storage.set("users",{
          "users":[]
               });

              } 
        
            });
        
 console.log("storage ends in this place")
this.updatesidemenu();




    });
  }

  ngOnInit() {

 this.camserv.savedupdatedlistener().subscribe(val=>{
if(val=="added"){

this.sucess("Saved sucess fully");

}


 })


    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
