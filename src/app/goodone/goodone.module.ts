import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodonePageRoutingModule } from './goodone-routing.module';

import { GoodonePage } from './goodone.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodonePageRoutingModule
  ],
  declarations: [GoodonePage]
})
export class GoodonePageModule {}
