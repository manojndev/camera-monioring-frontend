import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GoodonePage } from './goodone.page';

describe('GoodonePage', () => {
  let component: GoodonePage;
  let fixture: ComponentFixture<GoodonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodonePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GoodonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
